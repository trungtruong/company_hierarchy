# Full stack development task #

# Web API
1. Description: The web api is developed using Laravel Framework 5.5.33

2. Install Laravel and create a new project: https://laravel.com/docs/4.2/installation

3. Serve the application on the PHP development server:

- Navigate to the project folder. In this case "src\hrtool"

- php artisan serve


# Web app
1. Description: The web application is developed using Angular 4

2. Install Angular and create a new project: https://angular.io/guide/quickstart

3. Serve the application:

- Navigate to the project folder. In this case "src\webapp\hrtool"

- ng serve