<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class EmployeeHierarchyTest extends TestCase
{
    public function testValidInputGivesCorrectOutput()
    {
        $response = $this->json(
            'POST',
            '/api/employee-hierarchy/structure',
            [
                "Pete" => "Nick",
                "Barbara" => "Nick",
                "Nick" => "Sophie",
                "Sophie" => "Jonas"
            ]);

        $response->assertStatus(Response::HTTP_OK)
            ->assertJson([
                "Jonas" => [[
                    "Sophie" => [[
                        "Nick" => [
                            [
                                "Pete" => []
                            ],
                            [
                                "Barbara" => []
                            ]

                        ]
                    ]]
                ]]
            ], true);
    }

    public function testInputWithCircularHierarchyGivesError()
    {
        $response = $this->json(
            'POST',
            '/api/employee-hierarchy/structure',
            [
                "Pete" => "Nick",
                "Barbara" => "Nick",
                "Nick" => "Sophie",
                "Sophie" => "Jonas",
                "Jonas" => "Barbara"
            ]);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error" => "Loop hierarchy detected: Nick => Sophie => Jonas => Barbara => Nick"]);
    }

    public function testInputWith_NoEmployee_GivesError()
    {
        $response = $this->json(
            'POST',
            '/api/employee-hierarchy/structure',
            []);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error" => "There is no employee"]);
    }


    public function testInputWith_EmptyName_GivesError()
    {
        $response = $this->json(
            'POST',
            '/api/employee-hierarchy/structure',
            [
                "Pete" => "",
                "" => "Nick",
                "Nick" => "Sophie",
                "Sophie" => "Jonas",
                "Jonas" => "Barbara"
            ]);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error" => "Employee name cannot be empty or white spaces"]);
    }

    public function testInputWith_WhiteSpacesName_GivesError()
    {
        $response = $this->json(
            'POST',
            '/api/employee-hierarchy/structure',
            [
                "Pete" => "Nick",
                "Barbara" => "Nick",
                "  " => "Sophie",
                "Sophie" => "Jonas",
                "Jonas" => "Barbara"
            ]);

        $response->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJson(["error" => "Employee name cannot be empty or white spaces"]);
    }
}
