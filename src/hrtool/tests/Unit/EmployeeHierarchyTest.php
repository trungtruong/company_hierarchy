<?php

namespace Tests\Unit;

use App\Exceptions\EmployeeHierarchyException;
use App\Models\Employee;
use App\Services\EmployeeHierarchyService;
use Tests\TestCase;

class EmployeeHierarchyTest extends TestCase
{
    private function createHierarchyFromEmployee($name, array $subordinates): Employee
    {
        $employee = new Employee($name);

        if (!empty($subordinates)) {
            foreach ($subordinates as $key => $value) {
                $employee->addSubordinate($this->createHierarchyFromEmployee($key, $value));
            }
        }

        return $employee;
    }

    private function compareEmployee(Employee $expected, Employee $actual)
    {
        $this->assertEquals($expected->getName(), $actual->getName());

        $this->assertEquals(count($expected->getSubordinates()), count($actual->getSubordinates()));

        for ($index = 0; $index < count($expected->getSubordinates()); $index++) {
            $this->compareEmployee($expected->getSubordinates()[$index], $actual->getSubordinates()[$index]);
        }
    }

    // Test valid input

    /**
     * @param $input
     * @param $expectedTopEmployee
     * @throws EmployeeHierarchyException
     */
    private function assertValidInput_GivesCorrectOutput($input, $expectedTopEmployee)
    {
        $employeeHierarchyService = new EmployeeHierarchyService();

        $topEmployee = $employeeHierarchyService->findTopEmployee($input);

        $this->compareEmployee($expectedTopEmployee, $topEmployee);
    }

    /**
     * @throws EmployeeHierarchyException
     */
    public function testValidInput_TopEmployeeDeclaredFirst_GivesCorrectOutput()
    {
        $expectedTopEmployee = $this->createHierarchyFromEmployee("Jonas", array(
                "Sophie" => array(
                    "Nick" => array(
                        "Pete" => array(),
                        "Barbara" => array()
                    )
                )
            )
        );

        $input = array(
            "Sophie" => "Jonas",
            "Pete" => "Nick",
            "Barbara" => "Nick",
            "Nick" => "Sophie"
        );

        $this->assertValidInput_GivesCorrectOutput($input, $expectedTopEmployee);
    }

    /**
     * @throws EmployeeHierarchyException
     */
    public function testValidInput_TopEmployeeDeclaredInTheMiddle_GivesCorrectOutput()
    {
        $expectedTopEmployee = $this->createHierarchyFromEmployee("Jonas", array(
                "Sophie" => array(
                    "Nick" => array(
                        "Pete" => array(),
                        "Barbara" => array()
                    )
                )
            )
        );

        $input = array(
            "Pete" => "Nick",
            "Sophie" => "Jonas",
            "Barbara" => "Nick",
            "Nick" => "Sophie"
        );

        $this->assertValidInput_GivesCorrectOutput($input, $expectedTopEmployee);
    }

    /**
     * @throws EmployeeHierarchyException
     */
    public function testValidInput_TopEmployeeDeclaredLast_GivesCorrectOutput()
    {
        $expectedTopEmployee = $this->createHierarchyFromEmployee("Jonas", array(
                "Sophie" => array(
                    "Nick" => array(
                        "Pete" => array(),
                        "Barbara" => array()
                    )
                )
            )
        );

        $input = array(
            "Pete" => "Nick",
            "Barbara" => "Nick",
            "Nick" => "Sophie",
            "Sophie" => "Jonas"
        );

        $this->assertValidInput_GivesCorrectOutput($input, $expectedTopEmployee);
    }


    // Test input contains circular hierarchy

    /**
     * @param $input
     * @param $errorMessage
     * @throws EmployeeHierarchyException
     */
    private function assertThatFindingTopEmployee_WithInvalidInput_GivesError($input, $errorMessage)
    {
        $employeeHierarchyService = new EmployeeHierarchyService();

        $this->expectException(EmployeeHierarchyException::class);
        $this->expectExceptionMessage($errorMessage);
        $employeeHierarchyService->findTopEmployee($input);
    }

    /**
     * @throws EmployeeHierarchyException
     */
    public function testInputWithCircularHierarchy_OneLevel_GivesError()
    {
        $input = array(
            "Pete" => "Nick",
            "Nick" => "Pete"
        );

        $this->assertThatFindingTopEmployee_WithInvalidInput_GivesError($input, "Loop hierarchy detected: Pete => Nick => Pete");
    }

    /**
     * @throws EmployeeHierarchyException
     */
    public function testInputWithCircularHierarchy_TwoLevel_GivesError()
    {
        $input = array(
            "Pete" => "Nick",
            "Barbara" => "Nick",
            "Nick" => "Sophie",
            "Sophie" => "Barbara"
        );

        $this->assertThatFindingTopEmployee_WithInvalidInput_GivesError($input, "Loop hierarchy detected: Nick => Sophie => Barbara => Nick");
    }

    /**
     * @throws EmployeeHierarchyException
     */
    public function testInputWithCircularHierarchy_TopToBottom_GivesError()
    {
        $input = array(
            "Pete" => "Nick",
            "Barbara" => "Nick",
            "Nick" => "Sophie",
            "Sophie" => "Jonas",
            "Jonas" => "Pete"
        );

        $this->assertThatFindingTopEmployee_WithInvalidInput_GivesError($input, "Loop hierarchy detected: Pete => Nick => Sophie => Jonas => Pete");
    }

    /**
     * @throws EmployeeHierarchyException
     */
    public function testInputWithCircularHierarchy_LocatedBeforeValidHierarchy_GivesError()
    {
        $input = array(
            "Pete" => "Nick",
            "Sophie" => "Nick",
            "Nick" => "Pete",
            "Nicky" => "Barbara"
        );

        $this->assertThatFindingTopEmployee_WithInvalidInput_GivesError($input, "Loop hierarchy detected: Pete => Nick => Pete");
    }

    /**
     * @throws EmployeeHierarchyException
     */
    public function testInputWithCircularHierarchy_LocatedAfterValidHierarchy_GivesError()
    {
        $input = array(
            "Pete" => "Nicky",
            "Barbara" => "Nick",
            "Nick" => "Barbara"
        );

        $this->assertThatFindingTopEmployee_WithInvalidInput_GivesError($input, "Loop hierarchy detected: Barbara => Nick => Barbara");
    }


    // Test input contains multiple potential top employees

    /**
     * @throws EmployeeHierarchyException
     */
    public function testInputWith_2PotentialTopEmployees_GivesError()
    {
        $input = array(
            "Pete" => "Nick",
            "Barbara" => "Nicky",
            "Nick" => "Sophie",
            "Sophie" => "Jonas"
        );

        $this->assertThatFindingTopEmployee_WithInvalidInput_GivesError($input, "There are more than 1 top employee: Nicky, Jonas");
    }

    /**
     * @throws EmployeeHierarchyException
     */
    public function testInputWith_4PotentialTopEmployees_GivesError()
    {
        $input = array(
            "Pete" => "Nick",
            "Barbara" => "Nicky",
            "Nick" => "Trung",
            "Sophie" => "Jonas",
            "Michel" => "Gorge"
        );

        $this->assertThatFindingTopEmployee_WithInvalidInput_GivesError($input, "There are more than 1 top employee: Nicky, Trung, Jonas, Gorge");
    }

}
