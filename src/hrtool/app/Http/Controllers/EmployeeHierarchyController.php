<?php

namespace App\Http\Controllers;

use App\Exceptions\EmployeeHierarchyException;
use App\Services\EmployeeHierarchyService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmployeeHierarchyController extends Controller
{
    private $employeeHierarchyService;

    public function __construct(EmployeeHierarchyService $employeeHierarchyService)
    {
        $this->employeeHierarchyService = $employeeHierarchyService;
    }

    public function structure(Request $request)
    {
        try {
            $this->employeeHierarchyService->validateInputFormat($request->input());

            $topEmployee = $this->employeeHierarchyService->findTopEmployee($request->input());

            return response()->json($topEmployee, Response::HTTP_OK);
        } catch (EmployeeHierarchyException $e) {
            return response()->json(["error" => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
