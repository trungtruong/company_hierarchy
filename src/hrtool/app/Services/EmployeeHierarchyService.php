<?php
/**
 * Created by PhpStorm.
 * User: Trung
 * Date: 2/3/2018
 * Time: 12:32 PM
 */

namespace App\Services;

use App\Exceptions\EmployeeHierarchyException;
use App\Models\Employee;


class EmployeeHierarchyService
{
    private function findOrCreateEmployee($employeeName, &$employees): Employee
    {
        if (!array_key_exists($employeeName, $employees)) {
            $employees[$employeeName] = new Employee($employeeName);
        }

        return $employees[$employeeName];
    }

    /**
     * @param array $employees
     * @throws EmployeeHierarchyException
     */
    private function assertHasNoCircularHierarchy(array $employees): void
    {
        if (empty($employees)) {
            return;
        }

        // Make sure that the check goes through all employees
        $notYetAssertedEmployees = $employees;

        // Take the first employee as an entry point and continue searching up for the supervisor
        $employee = $employees[array_keys($employees)[0]];
        $name = $employee->getName();

        $employeeNames = [$name];
        unset($notYetAssertedEmployees[$name]);

        while (!is_null($employee->getSupervisor())) {
            $employee = $employee->getSupervisor();
            $name = $employee->getName();

            if (key_exists($name, $notYetAssertedEmployees)) {
                unset($notYetAssertedEmployees[$name]);
            }

            if (in_array($name, $employeeNames)) {
                throw new EmployeeHierarchyException('Loop hierarchy detected: ' . $this->buildCircularHierarchyString($employee));
            }

            $employeeNames[] = $name;
        }

        $this->assertHasNoCircularHierarchy($notYetAssertedEmployees);
    }

    /**
     * @param array $employees
     * @throws EmployeeHierarchyException
     */
    private function assertHasOnlyOneTopEmployee(array $employees): void
    {
        if (empty($employees)) {
            return;
        }

        $topEmployees = [];

        foreach ($employees as $employee) {
            if (is_null($employee->getSupervisor())) {
                $topEmployees[] = $employee->getName();
            }
        }

        if (count($topEmployees) > 1) {
            throw new EmployeeHierarchyException("There are more than 1 top employee: " . implode(', ', $topEmployees));
        }
    }

    private function buildCircularHierarchyString(Employee $employeeInLoop)
    {
        $employee = $employeeInLoop;
        $employeeNames = [$employee->getName()];

        while (!is_null($employee->getSupervisor())) {
            $employee = $employee->getSupervisor();
            $name = $employee->getName();

            if (in_array($name, $employeeNames)) {
                break;
            }

            $employeeNames[] = $name;
        }

        $loop = "";
        foreach ($employeeNames as $name) {
            $loop .= $name . " => ";
        }

        return $loop . $employeeInLoop->getName();
    }

    /**
     * @param array $input
     * @throws EmployeeHierarchyException
     */
    private function assertNotEmptyEmployeeList(array $input)
    {
        if (empty($input)) {
            throw new EmployeeHierarchyException("There is no employee");
        }
    }

    /**
     * @param array $input
     * @return array
     * @throws EmployeeHierarchyException
     */
    public function buildEmployeeList(array $input): array
    {
        $this->assertNotEmptyEmployeeList($input);

        $employees = [];

        foreach ($input as $key => $value) {
            $subordinateName = trim($key);
            $supervisorName = trim($value);

            $subordinate = $this->findOrCreateEmployee($subordinateName, $employees);
            $supervisor = $this->findOrCreateEmployee($supervisorName, $employees);

            $supervisor->addSubordinate($subordinate);
        }

        return $employees;
    }

    /**
     * @param array $employees
     * @return mixed|null
     * @throws EmployeeHierarchyException
     */
    public function findTopEmployeeFromEmployeeList(array $employees)
    {
        $this->assertHasNoCircularHierarchy($employees);
        $this->assertHasOnlyOneTopEmployee($employees);

        foreach ($employees as $employee) {
            if (is_null($employee->getSupervisor())) {
                return $employee;
            }
        }

        throw new EmployeeHierarchyException('Top employee not found');
    }

    /**
     * @param array $input
     * @return mixed|null
     * @throws EmployeeHierarchyException
     */
    public function findTopEmployee(array $input)
    {
        $employees = $this->buildEmployeeList($input);

        return $this->findTopEmployeeFromEmployeeList($employees);
    }


    // Validation functions

    /**
     * @param $name
     * @throws EmployeeHierarchyException
     */
    private function assertNameIsNotEmptyOrWhiteSpace($name)
    {
        if (ctype_space($name) || $name == '') {
            throw new EmployeeHierarchyException('Employee name cannot be empty or white spaces');
        }
    }

    /**
     * @param array $input
     * @throws EmployeeHierarchyException
     */
    public function validateInputFormat(array $input)
    {
        foreach ($input as $key => $value) {
            $this->assertNameIsNotEmptyOrWhiteSpace($key);
            $this->assertNameIsNotEmptyOrWhiteSpace($value);
        }
    }
}