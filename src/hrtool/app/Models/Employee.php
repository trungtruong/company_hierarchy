<?php
/**
 * Created by PhpStorm.
 * User: Trung
 * Date: 2/2/2018
 * Time: 11:52 PM
 */

namespace App\Models;


class Employee implements \JsonSerializable
{
    private $name;

    private $supervisor;
    private $subordinates;

    public function __construct($name)
    {
        $this->name = $name;
        $this->supervisor = NULL;
        $this->subordinates = [];
    }

    public function jsonSerialize()
    {
        return [$this->name => $this->subordinates];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSupervisor()
    {
        return $this->supervisor;
    }

    public function setSupervisor($supervisor)
    {
        $this->supervisor = $supervisor;
    }

    public function getSubordinates(): array
    {
        return $this->subordinates;
    }

    public function addSubordinate(Employee $employee)
    {
        $employee->setSupervisor($this);
        $this->subordinates[] = $employee;
    }
}