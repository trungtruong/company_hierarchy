import { Injectable } from "@angular/core";
import { Http, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { AppConstants } from "../constants.app";


@Injectable()
export class EmployeeHierarchyService {

    constructor(private http: Http) {

    }

    public getHierarchy(input: string): Observable<string> {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        let response: Observable<Response> = this.http.post(AppConstants.API_URL + 'employee-hierarchy/structure',  input , { headers });

        return response.map((res) => res.json()).catch((e: any) =>  Observable.throw(e.json().error || 'Server error'));
    }
}

