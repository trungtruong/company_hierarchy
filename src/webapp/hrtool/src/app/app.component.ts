import { Component, ViewChild } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { ITreeOptions } from 'angular-tree-component';

import { EmployeeHierarchyService } from 'app/services/employee-hierarchy.service';
import { makeDecorator } from '@angular/core/src/util/decorators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [EmployeeHierarchyService]
})

export class AppComponent {
  private file = null;
  private errorMessage = '';
  private isLoading: boolean = false;

  private obsEmployeeHierarchy: Observable<string>;

  private nodes = [];
  private options: ITreeOptions = {};

  @ViewChild('tree') tree;

  constructor(private employeeHierarchyService: EmployeeHierarchyService) {

  }

  handleUpload(event) {
    this.file = null;

    if(event.target.files && event.target.files.length > 0) {
      this.file = event.target.files[0];
    }
    
  }

  viewHierarchy() {
    this.errorMessage = '';
    this.nodes = [];

    if (this.file == null) {
      this.errorMessage = 'Please load a file first and press the button';
      return;
    }
    
    this.showLoadingIndicator()
    let reader = new FileReader();

    reader.readAsText(this.file);
    reader.onload = () => {
      this.obsEmployeeHierarchy = this.employeeHierarchyService.getHierarchy(reader.result);   
      this.obsEmployeeHierarchy.subscribe((m) => this.showHierarchy(m), (e: any) => this.handleError(e));   
    };
  }
  
  private showLoadingIndicator() {
    this.isLoading = true;
  }

  private hideLoadingIndicator() {
    this.isLoading = false;
  }

  private handleError(errorMessage) {
    this.hideLoadingIndicator();
    this.errorMessage = 'ERROR: ' + errorMessage;
  }

  private showHierarchy(jsonContent) {   
    for (var root in jsonContent) {

      let node: any = {
        name: root,
        children: this.loop(jsonContent[root])
      };

      this.nodes = [node];

      this.expandTreeView();  
    }
  }

  private expandTreeView() {
      // The treeview is not immediately expanded after the data gets updated.
      // This is probably because the UI rendering. 
      // SOLUTION: Set a timeout to expand the treeview
      setTimeout(() => {
        this.hideLoadingIndicator();
        this.tree.treeModel.expandAll();
      }, 200);
  }

  private loop(items): any {
    let childrens = new Array();
    
    items.forEach(item => {
      for (var i in item) {
        childrens.push({
          name: i,
          children: this.loop(item[i])
        });
      }
    });

    return childrens;
  }
  
}
